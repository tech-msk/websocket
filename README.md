# WebSocket 채팅 프로그램

> 이 프로젝트는 WebSoket 채팅 프로그램입니다.

### 개발환경

   1. 개발 언어 : Java, JDK 1.8, Spring Boot 2.1.5

        Default Package는 com.websocket.chatting 입니다.

                          기본 채팅 페이지만 개발되어있습니다.
                          카카오톡 페이지 처럼 만드는 것이 목표입니다.

### TODO LIST
        * 로그인
        * 친구목록 / 채팅목록 탭
        * 채팅 내용 유지 등