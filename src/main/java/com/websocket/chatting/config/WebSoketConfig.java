package com.websocket.chatting.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

import com.websocket.chatting.handler.SocketTextHandler;

/**
 * 소켓 설정
 * @since   2019. 11. 28
 * @author  민승기
 * @history 민승기 [2019. 11. 28] [최초 작성]
 */
@Configuration
@EnableWebSocket
public class WebSoketConfig implements WebSocketConfigurer{

  /**
   * 소켓 메시지 핸들러 객체
   */
  private SocketTextHandler socketTextHandler;

  /**
   * 소켓 메시지 핸들러 객체 주입
   * @param socketTextHandler - 소켓 메시지 핸들러 객체
   */
  @Autowired
  public void setSocketTextHandler(SocketTextHandler socketTextHandler) {
    this.socketTextHandler = socketTextHandler;
  }
  
  /**
   * 소켓 메시지 핸들러 객체 등록
   */
  @Override
  public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
    registry.addHandler(socketTextHandler, "/chat");
  }

}
